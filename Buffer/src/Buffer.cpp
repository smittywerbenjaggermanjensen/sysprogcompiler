/*
 * Buffer.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: negi1011
 */

#include "../includes/Buffer.h"
#include "../includes/CustomChar.h"


/*
 * Creates a Buffer with a given file path
 */
Buffer::Buffer(char* filePath) {
	buffer1 = new char[BUFFER_LENGTH];
	buffer2 = new char[BUFFER_LENGTH];

	next = buffer1;
	column = 1;
	row = 1;
	backLoaded = false;

	fileStream.open(filePath, std::ifstream::in);
	fillBuffer(buffer1);
}

/*
 * Deletes the buffer and closes the source file
 */
Buffer::~Buffer() {
	delete[] buffer1;
	delete[] buffer2;
	next = 0;
	column = 0;
	row = 0;
	fileStream.close();
}

/**
 * Returns the next Char from the buffer
 */
CustomChar* Buffer::getChar() {

	CustomChar* character = new CustomChar(*next, column, row);

	if (*next == '\n') {
		row++;
		column = 1;
	} else {
		column++;
	}

	setPointerToNextChar();

	return character;
}

/*
 * Prepares the pointer onto the next Char in the buffer
 */
void Buffer::setPointerToNextChar() {

	next++;

	if (*next == '\0') {

		if (next == buffer1 + BUFFER_LENGTH) {
			fillBuffer(buffer2);
			next = buffer2;
		}

		else if (next == buffer2 + BUFFER_LENGTH) {
			fillBuffer(buffer1);
			next = buffer1;
		}
	}
}

/*
 * Sets the Char back into the buffer
 */
void Buffer::ungetChar(CustomChar* character) {
	if (column == 1 && row == 1) {
		std::cerr << "Noch kein Zeichen gelesen. Zeichen kann nicht zurueckgegeben werden." << std::endl;
	}

	else {
		setPointerToLastChar();
		row = (*character).getRow();
		column = (*character).getColumn();
	}
}

/*
 * Uses a given number to set back a given amount of chars into the buffer
 */
void Buffer::ungetChar(CustomChar* lastCharInLexem, int count) {
	for (int i = 0; i < count; i++) {
		setPointerToLastChar();
	}
	row = (*lastCharInLexem).getRow();
	column = (*lastCharInLexem).getColumn();
}

/*
 * Prepares the pointer onto the next Char in the buffer
 */
void Buffer::setPointerToLastChar() {

	if (next == buffer1) {
		next = buffer2 + BUFFER_LENGTH - 1;
		backLoaded = true;
	}

	else if (next == buffer2) {
		next = buffer1 + BUFFER_LENGTH - 1;
		backLoaded = true;
	}

	else {
		next--;
	}
}

/*
 * Fills the Buffer with data from the Source Code File
 */
void Buffer::fillBuffer(char* buffer) {
    if (backLoaded) {
        backLoaded = false;
        return;
    }


	char c;
	int count = 0;

	while (count < BUFFER_LENGTH) {

		if (fileStream.get(c)) {
			*(buffer + count) = c;
		}

		else {
			*(buffer + count) = '\0';
		}

		count++;
	}
}

