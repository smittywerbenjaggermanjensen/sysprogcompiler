/*
 * CustomChar.cpp
 *
 *  Created on: Mar, 03 2017
 *      Author: negi1011
 */

#include "../includes/CustomChar.h"

// Constructor
CustomChar::CustomChar(char _character, int _column, int _row) {
	character = _character;
	column = _column;
	row = _row;
}

CustomChar::~CustomChar(){
	character = 0;
	column = 0;
	row = 0;
}

// Getter
char CustomChar::getChar() {
	return character;
}

unsigned int CustomChar::getColumn() {
	return column;
}

unsigned int CustomChar::getRow() {
	return row;
}
