#include <iostream>

#include "../includes/Buffer.h"
#include "../includes/CustomChar.h"

using namespace std;

int main(int argc, char **argv) {
	
	if (argc < 2) {
		cout << "File needed" << endl;
	} else {
		Buffer* buffer = new Buffer(argv[1]);
		CustomChar* cha = buffer->getChar();
		int counter = 0;

		while (cha->getChar() != '\0'){
			cout << cha->getChar();
			cha = buffer->getChar();
			counter++;
		}

		cout << "Character Count: " << counter << " Lines: " << cha->getRow() << endl;
	}
}
