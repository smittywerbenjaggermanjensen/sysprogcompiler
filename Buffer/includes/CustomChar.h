/**
 * This class is an extended Char Class
 */

#ifndef CUSTOM_CHAR_
#define CUSTOM_CHAR_

class CustomChar {
	private:
		char character;
		unsigned int column, row;

	public:
		CustomChar(char _character, int _column, int _row);
		virtual ~CustomChar();
		char getChar(void);
		unsigned int getColumn(void);
		unsigned int getRow(void);
};


#endif /* CUSTOM_CHAR */






