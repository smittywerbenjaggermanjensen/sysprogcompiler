#ifndef BUFFER_H_
#define BUFFER_H_
#define BUFFER_LENGTH 1024

#include "../includes/CustomChar.h"
#include <iostream>
#include <fstream>

class Buffer {
public:
	Buffer(char* filePath);
	virtual ~Buffer();
	CustomChar* getChar();
	void ungetChar(CustomChar* character);
	void ungetChar(CustomChar* lastCharInLexem, int count);
	void fillBuffer(char* buffer);

private:
	char* buffer1;
	char* buffer2;
	char* next;
	bool backLoaded;
	unsigned int column;
	unsigned int row;
	std::ifstream fileStream;
	void setPointerToNextChar();
	void setPointerToLastChar();
};

#endif /* BUFFER_H_ */
