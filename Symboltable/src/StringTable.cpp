/*
 * StringTable.cpp
 *
 *  Created on: May 5, 2017
 *      Author: henning
 */

#include "../includes/StringTable.h"

StringTable::StringTable() {

	first = new StringTableNode(DEFAULT_STRINGTABLENODE_SIZE);
	last = first;
	freeSpace = DEFAULT_STRINGTABLENODE_SIZE;
	nextFreeElement = first->getContent();
}

StringTable::~StringTable() {
	while (last != 0) {
		removeLastNode();
	}
}

/**
 *	Copies the passed lexem into the StringTable and returns a pointer to that adress.
 */
char* StringTable::insert(const char* lexem, unsigned int lexemLength) {
	if (lexemLength + 1 > freeSpace) {	// Take the trailing '\0' into consideration.
		if (lexemLength + 1 > DEFAULT_STRINGTABLENODE_SIZE) {
			createNewNode(lexemLength + 1);
		}
		else {
			createNewNode(DEFAULT_STRINGTABLENODE_SIZE);
		}
	}

	char* startOfInsertedLexem = nextFreeElement;

	memcpy(nextFreeElement, lexem, lexemLength);
	nextFreeElement[lexemLength] = '\0';
	nextFreeElement += lexemLength + 1;
	freeSpace -= lexemLength + 1;

	return startOfInsertedLexem;
}

void StringTable::createNewNode(unsigned int size) {
	StringTableNode* newNode = new StringTableNode(size);
	nextFreeElement = newNode->getContent();
	freeSpace = size;

	if (first != last) {
		newNode->setPrev(last);
		last->setNext(newNode);
		last = newNode;
	} else {
		last = newNode;
		first->setNext(last);
		newNode->setPrev(first);
	}
}

void StringTable::removeLastNode() {
	if (first != last) {
		StringTableNode* newLast = last->getPrev();
		newLast->setNext(0);
		delete last;
		last = newLast;
	} else {
		delete first;
		first = 0;
		last = 0;
	}
}
