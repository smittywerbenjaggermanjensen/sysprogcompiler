/*
 * InformationDoublyLinkedList.cpp
 *
 *  Created on: May 7, 2017
 *      Author: henning
 */

#include "../includes/InformationDoublyLinkedList.h"

InformationDoublyLinkedList::InformationDoublyLinkedList() {
	first = 0;
	last = 0;
	size = 0;
}

InformationDoublyLinkedList::~InformationDoublyLinkedList() {
	while (last != 0) {
		deleteLast();
	}
}

void InformationDoublyLinkedList::addNewInformation(const char* const lexem, const unsigned int lexemLength, Token::TokenType tokenType) {
	InformationDoublyLinkedListElement* newListElement = new InformationDoublyLinkedListElement(new Information(lexem, lexemLength, tokenType));
	if (size == 0) {
		first = newListElement;
		last = first;
	}
	else {
		newListElement->setPrev(last);
		last->setNext(newListElement);
		last = newListElement;
	}
	size++;
}

Information* InformationDoublyLinkedList::get(unsigned int index) {
	if (index >= size || index < 0) {
		throw "Error, index out of range.";
	}

	InformationDoublyLinkedListElement* iterator = first;
	for (unsigned int i = 0; i < index; i++) {
		iterator = iterator->getNext();
	}

	return iterator->getInformation();
}

void InformationDoublyLinkedList::deleteLast() {
	if (size == 0) {
		return;
	}
	else if (size == 1) {
		delete last;
		first = 0;
		last = 0;
		size--;
	}
	else {
		InformationDoublyLinkedListElement* newLastElement = last->getPrev();
		newLastElement->setNext(0);
		delete last;
		last = newLastElement;
		size--;
	}
}

unsigned int InformationDoublyLinkedList::getSize() {
	return size;
}
