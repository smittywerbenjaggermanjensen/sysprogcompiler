/*
 * SymbolTableKey.cpp
 *
 *  Created on: May 5, 2017
 *      Author: henning
 */

#include "../includes/SymbolTableKey.h"

SymbolTableKey::SymbolTableKey(const unsigned int _symbolTableIndex, const char *const _lexem,
                               const unsigned int _lexemLength)
        : symbolTableIndex(_symbolTableIndex), lexem(_lexem), lexemLength(_lexemLength) {
}

SymbolTableKey::~SymbolTableKey() {
}

const unsigned int SymbolTableKey::getSymbolTableIndex() {
    return symbolTableIndex;
}

const char *const SymbolTableKey::getLexem() {
    return lexem;
}

const unsigned int SymbolTableKey::getLexemLength() {
    return lexemLength;
}
