/*
 * InformationDoublyLinkedListElement.cpp
 *
 *  Created on: May 7, 2017
 *      Author: henning
 */

#include "../includes/InformationDoublyLinkedListElement.h"

InformationDoublyLinkedListElement::InformationDoublyLinkedListElement(Information* information) {
	this->information = information;
	next = 0;
	prev = 0;
}

InformationDoublyLinkedListElement::~InformationDoublyLinkedListElement() {
	next = 0;
	prev = 0;
}


Information* InformationDoublyLinkedListElement::getInformation() {
	return information;
}

InformationDoublyLinkedListElement* InformationDoublyLinkedListElement::getNext() {
	return next;
}

void InformationDoublyLinkedListElement::setNext(InformationDoublyLinkedListElement* next) {	// Use references here?
	this->next = next;
}

InformationDoublyLinkedListElement* InformationDoublyLinkedListElement::getPrev() {
	return prev;
}

void InformationDoublyLinkedListElement::setPrev(InformationDoublyLinkedListElement* prev) {	// Use references here?
	this->prev = prev;
}
