#include "../includes/Symboltable.h"

Symboltable::Symboltable() {
	stringTable = new StringTable();
	symbolTable =  new InformationDoublyLinkedList*[DEFAULT_SYMBOLTABLE_SIZE];

	for (unsigned int i = 0; i < DEFAULT_SYMBOLTABLE_SIZE; i++) {
		symbolTable[i] = new InformationDoublyLinkedList();
	}
	initSymbols();
}

Symboltable::~Symboltable() {
	delete stringTable;
	delete[] symbolTable;
}

SymbolTableKey* Symboltable::insert(const char* lexem, unsigned int lexemLength, Token::TokenType tokenType) {
	// Inserts a new Lexem into the Symboltable
	//
	// create Key by Hashing the Lexem
	// Check if lexem already exists
	// if yes, return key to its information
	// if not, insert lexem into stringtable
	// 		   create new empty Information object
	// 		   add information to symtab-DLL
	// 		   return key to new Information object.


	SymbolTableKey* key = hash(lexem, lexemLength);

	Information* info = lookup(key);

	if (info == 0) {
		char* lexemInStringTab = stringTable->insert(lexem, lexemLength);
		getCollisionListForKey(key)->addNewInformation(lexemInStringTab, lexemLength, tokenType);
	}

	return key;
}

SymbolTableKey* Symboltable::hash(const char* lexem, unsigned int lexemLength) {
    unsigned int hashValue = 1;
    const char* lexemPointer = lexem;

    for (unsigned int i = 0; i < lexemLength; i++) {
        hashValue = 31 * hashValue + (unsigned int) lexemPointer[i];
    }

    hashValue = hashValue % DEFAULT_SYMBOLTABLE_SIZE;

    return new SymbolTableKey(hashValue, lexem, lexemLength);
}



Information* Symboltable::lookup(SymbolTableKey* key) {
	// Get index of the key
	// Get the DLL at that index
	// Traverse the list, until Information-Lexem equals Key-Lexem
	// Return pointer to that information

	InformationDoublyLinkedList* collisionList = getCollisionListForKey(key);

	for (unsigned int i = 0; i < collisionList->getSize(); i++) {
		Information* info = collisionList->get(i);
		if (compareKeyAndInformation(key, info)) {
			return info;
		}
	}

	return 0;
}

bool Symboltable::compareKeyAndInformation(SymbolTableKey* key, Information* information) {
	if (key->getLexemLength() != information->getLexemLength()) return false;

	char* keyLexem = (char*) key->getLexem();
	char* infoLexem = (char*) information->getLexem();

	if (*keyLexem == '\0' && *infoLexem == '\0') return true;

	// TODO: Is this compare working correctly? Why not use strcmp?
	while (*keyLexem != '\0' && *infoLexem != '\0') {
		if (*keyLexem != *infoLexem) return false;
		keyLexem++;
		infoLexem++;
	}
	return true;
}

InformationDoublyLinkedList* Symboltable::getCollisionListForKey(SymbolTableKey* key) {
	return symbolTable[key->getSymbolTableIndex()];
}

void Symboltable::initSymbols() {
    insert("WHILE", 5, Token::WHILE);
    insert("while", 5, Token::WHILE);

    insert("IF", 2, Token::IF);
    insert("if", 2, Token::IF);

    insert("ELSE", 4, Token::ELSE);
    insert("else", 4, Token::ELSE);

    insert("write", 5, Token::WRITE);

    insert("read", 4, Token::READ);

    insert("int", 3, Token::INT);
}
