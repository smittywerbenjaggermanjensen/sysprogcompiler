#include "../includes/Symboltable.h"
#include "../includes/SymbolTableKey.h"
#include "../includes/Information.h"
#include "../includes/InformationDoublyLinkedList.h"
#include "../includes/StringTable.h"
#include "../includes/StringTableNode.h"

#include <iostream>

int main(int argc, char **argv) {
	using namespace std;

//	cout << "Key initialized with 1, \"hallo\" , 5" << endl;
//	SymbolTableKey* key = new SymbolTableKey(1, "hallo", 5);
//	cout << "Index: " << key->getSymbolTableIndex() << endl;
//	cout << "Lexem: " <<key->getLexem() << endl;
//	cout << "LexemLength: " << key->getLexemLength() << endl;
//	delete key;
//	key = 0;
//
//	cout << "" << endl;
//
//	cout << "Information initialized with \"foo\", 3" << endl;
//	Information* info = new Information("foo", 3);		// "foo" should be the return value by StringTable
//	cout << "Lexem: " << info->getLexem() << endl;
//	cout << "Content: " << info->getIdentifierType() << endl;
//	cout << "LexemLength: " << info->getLexemLength() << endl;
//	delete info;
//	info = 0;
//
//	cout << "" << endl;
//
//	cout << "Creating new StringTable" << endl;
//	StringTable* stringTable = new StringTable();
//	stringTable->insert("FirstEntry", 10);
//	stringTable->insert("X", 1);
//	stringTable->insert("SecondEntry", 11);
//	stringTable->insert("TooLongThirdEntry", 17);
//	delete stringTable;
//	stringTable = 0;
//
//	cout << "" << endl;
//
//	cout << "Creating InformationDoublyLinkedList" << endl;
//	InformationDoublyLinkedList* infolist = new InformationDoublyLinkedList();
//	infolist->addNewInformation("hallo1", 6);
//	Information* information = infolist->get(0);
//	cout << information->getLexem() << endl;
//	cout << information->getIdentifierType() << endl;
//	information->setIdentifierType(777);
//	cout << information->getIdentifierType() << endl;
//	infolist->addNewInformation("hallo2", 6);
//	cout << infolist->get(1)->getLexem() << endl;
//	infolist->addNewInformation("hallo3", 6);
//	cout << infolist->get(2)->getLexem() << endl;
//	delete infolist;
//	infolist = 0;
//
//	cout << "" << endl;



	cout << "Creating Symboltable" << endl;
	Symboltable* symtab = new Symboltable();

	SymbolTableKey* key = symtab->insert("Hallo", 5, Token::TokenType::IDENTIFIER);
	cout << "--- Key ---" << endl;
	cout << "Index: " << key->getSymbolTableIndex() << endl;
	cout << "Lexem: " <<key->getLexem() << endl;
	cout << "LexemLength: " << key->getLexemLength() << endl;

	cout << "" << endl;

	Information* info = symtab->lookup(key);
	cout << "--- Information ---" << endl;
	cout << "Lexem: " << info->getLexem() << endl;
	cout << "Content: " << info->getIdentifierType() << endl;

    info->setIdentifierType(3);
	cout << "New set identifierType: " << info->getIdentifierType() << endl;

	cout << "" << endl;

	SymbolTableKey* key2 = symtab->insert("Hallo", 5, Token::TokenType::IDENTIFIER);
	cout << "--- Key2 ---" << endl;
	cout << "Index: " << key2->getSymbolTableIndex() << endl;
	cout << "Lexem: " << key2->getLexem() << endl;
	cout << "LexemLength: " << key2->getLexemLength() << endl;

	cout << "" << endl;

	Information* info2 = symtab->lookup(key2);
	cout << "--- Information2 ---" << endl;
	cout << "Lexem: " << info2->getLexem() << endl;
	cout << "Content: " << info2->getIdentifierType() << endl;

	cout << "" << endl;


	SymbolTableKey* key3 = symtab->insert("ollaH", 5, Token::TokenType::IDENTIFIER);
	cout << "--- Key3 ---" << endl;
	cout << "Index: " << key3->getSymbolTableIndex() << endl;
	cout << "Lexem: " <<key3->getLexem() << endl;
	cout << "LexemLength: " << key3->getLexemLength() << endl;

	cout << "" << endl;

	Information* info3 = symtab->lookup(key3);
	cout << "--- Information3 ---" << endl;
	cout << "Lexem: " << info3->getLexem() << endl;
	cout << "Content: " << info3->getIdentifierType() << endl;

	cout << "" << endl;

	SymbolTableKey* key4 = symtab->insert("Peter", 5, Token::TokenType::IDENTIFIER);
	cout << "--- Key4 ---" << endl;
	cout << "Index: " << key4->getSymbolTableIndex() << endl;
	cout << "Lexem: " <<key4->getLexem() << endl;
	cout << "LexemLength: " << key4->getLexemLength() << endl;

	cout << "" << endl;

	Information* info4 = symtab->lookup(key4);
	cout << "--- Information4 ---" << endl;
	cout << "Lexem: " << info4->getLexem() << endl;
	cout << "Content: " << info4->getIdentifierType() << endl;

	delete symtab;
	symtab = 0;

}
