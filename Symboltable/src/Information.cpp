/*
 * Information.cpp
 *
 *  Created on: May 5, 2017
 *      Author: henning
 */

#include "../includes/Information.h"

Information::Information(const char *const _lexem, const unsigned int _lexemLength, Token::TokenType _tokenType)
        : lexem(_lexem), lexemLength(_lexemLength), tokenType(_tokenType) {
    identifierType = 0;
}

Information::~Information() {
}

const char *const Information::getLexem() {
    return lexem;
}

const unsigned int Information::getLexemLength() {
    return lexemLength;
}

Token::TokenType Information::getTokenType() {
    return tokenType;
}

unsigned int Information::getIdentifierType() {
    return identifierType;
}

void Information::setIdentifierType(unsigned int _content) {
    identifierType = _content;
}
