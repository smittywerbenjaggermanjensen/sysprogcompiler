/*
 * StringTableNode.cpp
 *
 *  Created on: May 5, 2017
 *      Author: henning
 */

#include "../includes/StringTableNode.h"

StringTableNode::StringTableNode(const unsigned int size) {
	content = new char[size];
	prev = 0;
	next = 0;
}

StringTableNode::~StringTableNode() {
	delete[] content;
	prev = 0;
	next = 0;
}

char* StringTableNode::getContent() {
	return content;
}

StringTableNode* StringTableNode::getPrev() {
	return prev;
}

void StringTableNode::setPrev(StringTableNode* _prev) {
	prev = _prev;
}

StringTableNode* StringTableNode::getNext() {
	return next;
}

void StringTableNode::setNext(StringTableNode* _next) {
	next = _next;
}
