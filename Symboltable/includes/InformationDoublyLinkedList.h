/*
 * InformationDoublyLinkedList.h
 *
 *  Created on: May 7, 2017
 *      Author: henning
 */

#ifndef SYMBOLTABLE_INCLUDES_INFORMATIONDOUBLYLINKEDLIST_H_
#define SYMBOLTABLE_INCLUDES_INFORMATIONDOUBLYLINKEDLIST_H_

#include "../includes/InformationDoublyLinkedListElement.h"
#include "../includes/Information.h"

class InformationDoublyLinkedList {
public:
	InformationDoublyLinkedList();
	virtual ~InformationDoublyLinkedList();
	void addNewInformation(const char* const lexem, const unsigned int lexemLength, Token::TokenType tokenType);
	Information* get(unsigned int index);
	unsigned int getSize();

private:
	InformationDoublyLinkedListElement* first;
	InformationDoublyLinkedListElement* last;
	unsigned int size;

	void addLast(Information* information);
	void deleteLast();
};

#endif /* SYMBOLTABLE_INFORMATIONDOUBLYLINKEDLIST_H_ */
