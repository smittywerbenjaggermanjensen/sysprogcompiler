/*
 * StringTableNode.h
 *
 *  Created on: May 5, 2017
 *      Author: henning
 */

#ifndef SYMBOLTABLE_INCLUDES_STRINGTABLENODE_H_
#define SYMBOLTABLE_INCLUDES_STRINGTABLENODE_H_

class StringTableNode {
public:
	StringTableNode(const unsigned int size);
	virtual ~StringTableNode();

	char* getContent();

	StringTableNode* getPrev();
	void setPrev(StringTableNode* _prev);

	StringTableNode* getNext();
	void setNext(StringTableNode* _next);

private:
	char* content;

	StringTableNode* prev;
	StringTableNode* next;
};

#endif /* SYMBOLTABLE_STRINGTABLENODE_H_ */
