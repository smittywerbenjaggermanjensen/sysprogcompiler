#ifndef SYMBOLTABLE_H_
#define SYMBOLTABLE_H_

#include "SymbolTableKey.h"
#include "StringTable.h"
#include "Information.h"
#include "InformationDoublyLinkedList.h"

class Symboltable {
public:
    Symboltable();

    virtual ~Symboltable();

    SymbolTableKey *insert(const char *lexem, unsigned int lexemLength, Token::TokenType tokenType);

    Information *lookup(SymbolTableKey *key);

private:
    bool compareKeyAndInformation(SymbolTableKey *key, Information *info);

    static const unsigned int DEFAULT_SYMBOLTABLE_SIZE = 1024;
    StringTable *stringTable;
    InformationDoublyLinkedList **symbolTable;

    SymbolTableKey *hash(const char *lexem, unsigned int lexemLength);

    InformationDoublyLinkedList *getCollisionListForKey(SymbolTableKey *key);

    void initSymbols();
};

#endif /* SYMBOLTABLE_H_ */
