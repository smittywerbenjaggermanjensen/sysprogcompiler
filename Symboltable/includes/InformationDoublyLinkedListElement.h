/*
 * InformationDoublyLinkedListElement.h
 *
 *  Created on: May 7, 2017
 *      Author: henning
 */

#ifndef SYMBOLTABLE_INCLUDES_INFORMATIONDOUBLYLINKEDLISTELEMENT_H_
#define SYMBOLTABLE_INCLUDES_INFORMATIONDOUBLYLINKEDLISTELEMENT_H_

#include "../includes/Information.h"

class InformationDoublyLinkedListElement {
public:
	InformationDoublyLinkedListElement(Information* information);
	virtual ~InformationDoublyLinkedListElement();

	Information* getInformation();
	InformationDoublyLinkedListElement* getNext();
	void setNext(InformationDoublyLinkedListElement*);
	InformationDoublyLinkedListElement* getPrev();
	void setPrev(InformationDoublyLinkedListElement*);

private:
	Information* information;	// TODO Use pointer, value or copy to heap?
	InformationDoublyLinkedListElement* next;
	InformationDoublyLinkedListElement* prev;
};

#endif /* SYMBOLTABLE_SRC_INFORMATIONDOUBLYLINKEDLISTELEMENT_H_ */
