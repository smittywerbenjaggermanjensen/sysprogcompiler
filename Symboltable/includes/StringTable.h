/*
 * StringTable.h
 *
 *  Created on: May 5, 2017
 *      Author: henning
 */

#ifndef SYMBOLTABLE_INCLUDES_STRINGTABLE_H_
#define SYMBOLTABLE_INCLUDES_STRINGTABLE_H_

#include "../includes/StringTableNode.h"
#include <cstring>	// needed only for memcpy

class StringTable {
public:
	StringTable();
	virtual ~StringTable();
	char* insert(const char* lexem, unsigned int lexemLength);

private:
	static const unsigned int DEFAULT_STRINGTABLENODE_SIZE = 15;	// TODO find optimal value
	char* nextFreeElement;
	unsigned int freeSpace;

	StringTableNode* first;
	StringTableNode* last;

	void createNewNode(unsigned int size);
	void removeLastNode();
};

#endif /* SYMBOLTABLE_SRC_STRINGTABLE_H_ */
