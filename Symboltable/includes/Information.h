/*
 * Information.h
 *
 *  Created on: May 5, 2017
 *      Author: henning
 */

#ifndef SYMBOLTABLE_INCLUDES_INFORMATION_H_
#define SYMBOLTABLE_INCLUDES_INFORMATION_H_

#include "../../Automat/includes/Token.h"

class Information {
public:
	Information(const char* _lexem, const unsigned int lexemLength, Token::TokenType tokenType);
	virtual ~Information();

	const char* const getLexem();
	const unsigned int getLexemLength();
	Token::TokenType getTokenType();
	unsigned int getIdentifierType();
	void setIdentifierType(unsigned int _content);

private:
	const char* const lexem;	// will be stored in StringTable (HEAP)
	const unsigned int lexemLength;
	Token::TokenType tokenType;
	unsigned int identifierType;
};

#endif /* SYMBOLTABLE_SRC_INFORMATION_H_ */
