/*
 * SymbolTableKey.h
 *
 *  Created on: May 5, 2017
 *      Author: henning
 */

#ifndef SYMBOLTABLEKEY_H_
#define SYMBOLTABLEKEY_H_

class SymbolTableKey {
public:
    SymbolTableKey(unsigned int _symbolTableIndex, const char *_lexem, unsigned int _lexemLength);

    virtual ~SymbolTableKey();

    const unsigned int getSymbolTableIndex();

    const char *const getLexem();

    const unsigned int getLexemLength();

private:
    const unsigned int symbolTableIndex;
    const char *const lexem;        // TODO Save on heap, stack or just store pointer?
    const unsigned int lexemLength;
};

#endif /* SYMBOLTABLEKEY_H_ */
