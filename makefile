HEADERDIR = includes
SRCDIR = src
TARGETDIR = objs
DEBUGDIR = debug

AUTOMATDIR = Automat
BUFFERDIR = Buffer
SYMBOLTABLEDIR = Symboltable
SCANNERDIR = Scanner
PARSERDIR = Parser

all: parser testScanner
	@echo "Build all successfully"

parser: BufferOBJs SymboltableOBJs AutomatOBJs ScannerOBJs ParserExecutable
	@echo "Parser build successfully"

testScanner: BufferOBJs SymboltableOBJs AutomatOBJs ScannerOBJs TestScanner
	@echo "TestScanner build successfully"

cleanAll:
	@echo "clean all"
	rm -f $(AUTOMATDIR)/$(TARGETDIR)/*.o
	rm -f $(BUFFERDIR)/$(TARGETDIR)/*.o
	rm -f $(SYMBOLTABLEDIR)/$(TARGETDIR)/*.o
	rm -f $(SCANNERDIR)/$(TARGETDIR)/*.o
	rm -f $(PARSERDIR)/$(TARGETDIR)/*.o
	rm -f $(AUTOMATDIR)/$(DEBUGDIR)/*
	rm -f $(BUFFERDIR)/$(DEBUGDIR)/*
	rm -f $(SYMBOLTABLEDIR)/$(DEBUGDIR)/*
	rm -f $(SCANNERDIR)/$(DEBUGDIR)/*
	rm -f $(PARSERDIR)/$(DEBUGDIR)/*

AutomatOBJs:
	$(MAKE) -C $(AUTOMATDIR) AutomatOBJs
	
BufferOBJs:
	$(MAKE) -C $(BUFFERDIR) BufferOBJs
	
SymboltableOBJs:
	$(MAKE) -C $(SYMBOLTABLEDIR) SymboltableOBJs

ScannerOBJs:
	$(MAKE) -C $(SCANNERDIR) ScannerOBJs

TestScanner:
	$(MAKE) -C $(SCANNERDIR) makeTestScanner

ParserExecutable:
	$(MAKE) -C $(PARSERDIR) makeParserExecutable
