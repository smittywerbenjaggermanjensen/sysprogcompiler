#ifndef Automat_H_
#define Automat_H_

#include "Token.h"
#include "../../Buffer/includes/Buffer.h"

class Automat {
public:
    //static const int STATE_START = 0;
    //static const int STATE_IDENTIFIER = 1;
    //static const int STATE_INTEGER = 2;
    //static const int STATE_COLON = 3;
    //static const int STATE_EQUAL = 4;
    //static const int STATE_AMPERSANT = 5;
    //static const int STATE_COMMENTARY = 6;
    //static const int STATE_STARINCOMMENT = 7;
    //static const int STATE_EQUALCOLON = 8;
    typedef enum {
        STATE_START,
        STATE_IDENTIFIER,
        STATE_INTEGER,
        STATE_COLON,
        STATE_EQUAL,
        STATE_AMPERSANT,
        STATE_COMMENTARY,
        STATE_STARINCOMMENT,
        STATE_EQUALCOLON
    } automatstate;

    Automat(Buffer *buffer);

    virtual ~Automat();

    void insertChar(CustomChar *currCustomChar);

    unsigned int getCurrentState();

    bool isFinished();

    Token *getToken();

    int getState();

private:
    Buffer *buffer;

    unsigned int currentState;
    unsigned int lexemLength;
    unsigned int custCharPtrsLength;
    bool finished;
    char *lexem;
    unsigned int row, column;
    Token *token;

    static const int MAX_LEXEM_LENGTH = 256;
    CustomChar *custCharPointers[MAX_LEXEM_LENGTH];


    typedef enum {
        TYPE_UNKNOWN,
        TYPE_DIGIT,
        TYPE_SIGN,
        TYPE_SPACE,
        TYPE_LETTER,
        TYPE_EQUALS,
        TYPE_COLON,
        TYPE_AMPERSANT,
        TYPE_STAR
    } chartype;

    bool isDigit(char currentChar);

    bool isLetter(char currentChar);

    bool isStandaloneSign(char currentChar);

    bool isSpace(char currentChar);

    Token::TokenType getTokenTypeForStandaloneSign(char currentChar);

    void createToken(Token::TokenType tokentype);

    unsigned int getCurrentCharType(char currentChar);

    void addCharToLexem(char currentChar);

    void decreaseLexemByOne();

    void resetAutomat();

    void ungetCustomChar(int count);

    char *copyLexem();
};

#endif /* Automat_H_ */
