#ifndef TOKEN_H_
#define TOKEN_H_

#include "../../Symboltable/includes/SymbolTableKey.h"
#include <string>

class Token {
public:
    enum TokenType {
        UNKNOWN,
        ERROR,
        IDENTIFIER,
        NUMBER,
        STAR,
        COLON,
        SEMICOLON,
        EQUAL,
        PLUS,
        MINUS,
        GREATERTHAN,
        LESSTHAN,
        ASSIGN,
        WHILE,
        IF,
        ELSE,
        WRITE,
        READ,
        INT,
        EXCLAMATIONMARK,
        L_BRACE,
        L_PARANTHESIS,
        L_SQUARE,
        R_BRACE,
        R_PARANTHESIS,
        R_SQUARE,
        DOUBLEAMPERSANT,
        EQUALCOLONEQUAL,
        END_OF_FILE
    };

    Token();
    virtual ~Token();

    int getRow(void);
    int getColumn(void);
    long getNumber(void);
    char *getLexem(void);
    int getLexemLength(void);
    char *getSymbol(void);
    TokenType getTokenType();
    SymbolTableKey *getSymbolTableKey(void);

    void setRow(int);
    void setColumn(int);
    void setNumber(long);
    void setLexem(char *);
    void setLexemLength(int);
    void setSymbol(char *);
    void setTokenType(TokenType);
    void setSymbolTableKey(SymbolTableKey *);

    std::string getTokenTypeString(void);
    static std::string getTokenTypeString(TokenType tokenType);


private:
    TokenType tokentype;
    SymbolTableKey *symtabkey;
    int row;
    int column;

    long number;
    char *symbol;
    char *lexem;
    int lexemLength;
};

#endif /* Token_H_ */
