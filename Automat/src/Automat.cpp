#include "../includes/Automat.h"

#include <stdint.h>
#include <errno.h>
#include <cstring>
#include <stdlib.h>

Automat::Automat(Buffer *buffer) {
    this->buffer = buffer;
    lexem = new char[MAX_LEXEM_LENGTH];
    resetAutomat();
}

Automat::~Automat() {
    buffer = 0;
    row = 0;
    column = 0;
    lexemLength = 0;
    custCharPtrsLength = 0;
    delete[] lexem;
}

void Automat::insertChar(CustomChar *currCustomChar) {
    char currentChar = currCustomChar->getChar();

    custCharPointers[custCharPtrsLength] = currCustomChar;
    custCharPtrsLength++;

    if (lexemLength >= MAX_LEXEM_LENGTH - 1) {
        while (!isSpace(buffer->getChar()->getChar())) {
            lexemLength++;
        }
        std::cerr << "Scanner: " << "Lexemlength: " << lexemLength << " but maximum is " << MAX_LEXEM_LENGTH
                  << ". Row: " << row << " Column: " << column
                  << std::endl;
        createToken(Token::ERROR);
        return;
    }

    switch (getCurrentState()) {
        case STATE_START:
            row = currCustomChar->getRow();
            column = currCustomChar->getColumn();

            switch (getCurrentCharType(currentChar)) {

                case TYPE_LETTER:
                    addCharToLexem(currentChar);
                    currentState = STATE_IDENTIFIER;
                    break;
                case TYPE_DIGIT:
                    addCharToLexem(currentChar);
                    currentState = STATE_INTEGER;
                    break;
                case TYPE_COLON:
                    addCharToLexem(currentChar);
                    currentState = STATE_COLON;
                    break;
                case TYPE_EQUALS:
                    addCharToLexem(currentChar);
                    currentState = STATE_EQUAL;
                    break;
                case TYPE_AMPERSANT:
                    addCharToLexem(currentChar);
                    currentState = STATE_AMPERSANT;
                    break;
                case TYPE_SPACE:
                    currentState = STATE_START;
                    break;
                case TYPE_STAR:
                case TYPE_SIGN:
                case TYPE_UNKNOWN:
                    addCharToLexem(currentChar);
                    createToken(getTokenTypeForStandaloneSign(currentChar));
                    break;

            }
            break;

        case STATE_IDENTIFIER:
            switch (getCurrentCharType(currentChar)) {
                case TYPE_LETTER:
                case TYPE_DIGIT:
                    addCharToLexem(currentChar);
                    currentState = STATE_IDENTIFIER;
                    break;
                case TYPE_SPACE:
                    createToken(Token::IDENTIFIER);
                    break;
                case TYPE_COLON:
                case TYPE_EQUALS:
                case TYPE_SIGN:
                case TYPE_STAR:
                case TYPE_AMPERSANT:
                case TYPE_UNKNOWN:
                    ungetCustomChar(1);
                    createToken(Token::IDENTIFIER);
                    break;
            }
            break;

        case STATE_INTEGER:
            switch (getCurrentCharType(currentChar)) {
                case TYPE_DIGIT:
                    addCharToLexem(currentChar);
                    currentState = STATE_INTEGER;
                    break;
                case TYPE_SPACE:
                    createToken(Token::NUMBER);
                    break;
                case TYPE_LETTER: //Identifier can't start with a digit
                case TYPE_COLON:
                case TYPE_EQUALS:
                case TYPE_SIGN:
                case TYPE_STAR:
                case TYPE_AMPERSANT:
                case TYPE_UNKNOWN:
                    ungetCustomChar(1);
                    createToken(Token::NUMBER);
                    break;
            }
            break;

        case STATE_COLON:
            switch (getCurrentCharType(currentChar)) {
                case TYPE_EQUALS: // :=
                    addCharToLexem(currentChar);
                    createToken(Token::ASSIGN);
                    break;
                case TYPE_STAR: // :*
                    addCharToLexem(currentChar);
                    currentState = STATE_COMMENTARY;
                    break;
                case TYPE_SPACE:
                    createToken(Token::COLON);
                    break;
                case TYPE_COLON:
                case TYPE_LETTER:
                case TYPE_DIGIT:
                case TYPE_SIGN:
                case TYPE_AMPERSANT:
                case TYPE_UNKNOWN:
                    ungetCustomChar(1);
                    createToken(Token::COLON);
                    break;
            }
            break;

        case STATE_EQUAL:
            switch (getCurrentCharType(currentChar)) {
                case TYPE_COLON: // =:
                    addCharToLexem(currentChar);
                    currentState = STATE_EQUALCOLON;
                    break;
                case TYPE_SPACE:
                    createToken(Token::EQUAL);
                    break;
                case TYPE_LETTER:
                case TYPE_DIGIT:
                case TYPE_SIGN:
                case TYPE_STAR:
                case TYPE_EQUALS:
                case TYPE_AMPERSANT:
                case TYPE_UNKNOWN:
                    ungetCustomChar(1);
                    createToken(Token::EQUAL);
                    break;
            }
            break;

        case STATE_EQUALCOLON:
            switch (getCurrentCharType(currentChar)) {
                case TYPE_EQUALS: // =:=
                    addCharToLexem(currentChar);
                    createToken(Token::EQUALCOLONEQUAL);
                    break;
                case TYPE_COLON:
                case TYPE_LETTER:
                case TYPE_DIGIT:
                case TYPE_SIGN:
                case TYPE_STAR:
                case TYPE_AMPERSANT:
                case TYPE_UNKNOWN:
                case TYPE_SPACE:
                    decreaseLexemByOne();
                    ungetCustomChar(2);
                    createToken(Token::EQUAL);
                    break;
            }
            break;

        case STATE_AMPERSANT:
            switch (getCurrentCharType(currentChar)) {
                case TYPE_AMPERSANT: // &&
                    addCharToLexem(currentChar);
                    createToken(Token::DOUBLEAMPERSANT);
                    break;
                default:
                    //& is not a valid token
                    ungetCustomChar(1);
                    createToken(Token::ERROR);
                    break;
            }
            break;

        case STATE_COMMENTARY:
            switch (getCurrentCharType(currentChar)) {
                case TYPE_STAR:
                    currentState = STATE_STARINCOMMENT;
                    break;
                case TYPE_SPACE:
                    if (currentChar == '\0') {
                        currentState = STATE_START;
                    }
                    break;
                default:
                    currentState = STATE_COMMENTARY;
                    break;
            }
            break;

        case STATE_STARINCOMMENT:
            switch (getCurrentCharType(currentChar)) {
                case TYPE_COLON: // *:
                    resetAutomat();
                    break;

                case TYPE_STAR:
                    currentState = STATE_STARINCOMMENT;
                    break;

                default:
                    currentState = STATE_COMMENTARY;
                    break;

            }
    }
}

unsigned int Automat::getCurrentCharType(char currChar) {
    if (isDigit(currChar)) {
        return TYPE_DIGIT;
    }

    if (isLetter(currChar)) {
        return TYPE_LETTER;
    }

    if (isStandaloneSign(currChar)) {
        return TYPE_SIGN;
    }

    if (isSpace(currChar)) {
        return TYPE_SPACE;
    }

    if (currChar == ':') {
        return TYPE_COLON;
    }

    if (currChar == '&') {
        return TYPE_AMPERSANT;
    }

    if (currChar == '=') {
        return TYPE_EQUALS;
    }

    if (currChar == '*') {
        return TYPE_STAR;
    }

    return TYPE_UNKNOWN;
}

bool Automat::isDigit(char currChar) {
    return currChar >= '0' && currChar <= '9';
}

bool Automat::isLetter(char currChar) {
    return (currChar >= 'a' && currChar <= 'z')
           || (currChar >= 'A' && currChar <= 'Z');
}

bool Automat::isStandaloneSign(char currChar) {
    return currChar == ';' || currChar == '+' || currChar == '-'
           || currChar == '-' || currChar == '(' || currChar == ')'
           || currChar == '!' || currChar == '{' || currChar == '}'
           || currChar == '[' || currChar == ']' || currChar == '<'
           || currChar == '>';
}

bool Automat::isSpace(char currChar) {
    return currChar == ' ' || currChar == '\t' || currChar == '\n'
           || currChar == '\r' || currChar == '\0';
}

Token::TokenType Automat::getTokenTypeForStandaloneSign(char currentChar) {
    if (currentChar == '+') {
        return Token::PLUS;

    } else if (currentChar == '-' || currentChar == '-') {
        return Token::MINUS;

    } else if (currentChar == '!') {
        return Token::EXCLAMATIONMARK;

    } else if (currentChar == '>') {
        return Token::GREATERTHAN;

    } else if (currentChar == '<') {
        return Token::LESSTHAN;

    } else if (currentChar == '(') {
        return Token::L_PARANTHESIS;

    } else if (currentChar == ')') {
        return Token::R_PARANTHESIS;

    } else if (currentChar == '{') {
        return Token::L_BRACE;

    } else if (currentChar == '}') {
        return Token::R_BRACE;

    } else if (currentChar == '[') {
        return Token::L_SQUARE;

    } else if (currentChar == ']') {
        return Token::R_SQUARE;

    } else if (currentChar == '*') {
        return Token::STAR;

    } else if (currentChar == ';') {
        return Token::SEMICOLON;

    } else {
        return Token::UNKNOWN;
    }
}

char *Automat::copyLexem() {
    char *tempString = new char[lexemLength];
    for (unsigned int i = 0; i < lexemLength; i++) {
        tempString[i] = lexem[i];
    }
    return tempString;
}

void Automat::createToken(Token::TokenType tokentype) {
    this->token = new Token();
    token->setTokenType(tokentype);

    switch (tokentype) {
        case Token::IDENTIFIER:
            token->setLexem(copyLexem());
            token->setLexemLength(lexemLength);
            break;

        case Token::NUMBER:
            long value;
            errno = 0;
            value = strtol(lexem, NULL, 10);
            if (errno == ERANGE) {
                std::cerr << "Scanner: " << strerror(ERANGE) << ". Row: " << row << " Column: " << column << std::endl;
                token->setTokenType(Token::ERROR);
            } else {
                token->setNumber(value);
            }
            break;

        case Token::UNKNOWN:
            token->setSymbol(copyLexem());
            std::cerr << "Cant resolve char type for " << token->getSymbol() << " in row " << row << " column "  << column <<std::endl;
            break;
        case Token::ERROR:
            token->setSymbol(copyLexem());
            break;

        default:
            token->setSymbol(copyLexem());
            break;
    }

    token->setRow(row);
    token->setColumn(column);
    finished = true;
}

void Automat::resetAutomat() {
    currentState = STATE_START;
    finished = false;
    row = 0;
    column = 0;
    custCharPtrsLength = 0;
    lexemLength = 0;
    memset(lexem, 0, MAX_LEXEM_LENGTH);
}

void Automat::ungetCustomChar(int count) {
    for (int i = 1; i <= count; i++) {
        buffer->ungetChar(custCharPointers[custCharPtrsLength - i]);
    }
}

void Automat::addCharToLexem(char currentChar) {
    if (lexemLength < MAX_LEXEM_LENGTH) {
        lexem[lexemLength] = currentChar;
        lexemLength++;
    }
}

void Automat::decreaseLexemByOne() {
    lexem[--lexemLength] = '\0';
}

unsigned int Automat::getCurrentState() {
    return this->currentState;
}

bool Automat::isFinished() {
    return this->finished;
}

Token *Automat::getToken() {
    if (!isFinished()) {
        std::cerr << "getToken() is not available until Automat is finished!" << std::endl;
        return 0;
    }
    resetAutomat();
    return this->token;
}

int Automat::getState() {
    return currentState;
}
