#include "../includes/Token.h"

Token::Token() {
    this->tokentype = UNKNOWN;
    this->row = 0;
    this->column = 0;
    this->symtabkey = 0;
    this->lexem = 0;
    this->symbol = 0;
    this->number = 0;
    this->lexemLength = 0;
}

Token::~Token() {
    this->tokentype = UNKNOWN;
    this->column = 0;
    this->row = 0;
    this->symtabkey = 0;
    this->number = 0;
    this->symbol = 0;
    this->lexem = 0;
    this->lexemLength = 0;
}

void Token::setTokenType(TokenType tokentype) {
    this->tokentype = tokentype;
}

void Token::setColumn(int column) {
    this->column = column;
}

void Token::setRow(int row) {
    this->row = row;
}

void Token::setSymbolTableKey(SymbolTableKey *symtabkey) {
    this->symtabkey = symtabkey;
}

void Token::setNumber(long value) {
    this->number = value;
}

void Token::setSymbol(char *value) {
    this->symbol = value;
}

void Token::setLexem(char *lexem) {
    this->lexem = lexem;
}

void Token::setLexemLength(int length) {
    this->lexemLength = length;
}

Token::TokenType Token::getTokenType() {
    return this->tokentype;
}

int Token::getColumn() {
    return this->column;
}

int Token::getRow() {
    return this->row;
}

SymbolTableKey *Token::getSymbolTableKey() {
    return this->symtabkey;
}

long Token::getNumber() {
    return this->number;
}

char *Token::getSymbol() {
    return this->symbol;
}

char *Token::getLexem() {
    return this->lexem;
}

int Token::getLexemLength() {
    return this->lexemLength;
}

std::string Token::getTokenTypeString() {
    return getTokenTypeString(this->getTokenType());
}

std::string Token::getTokenTypeString(TokenType tokenType) {
    switch (tokenType) {
        case UNKNOWN:   return "UNKNOWN";
        case ERROR: return "ERROR";
        case IDENTIFIER: return "IDENTIFIER";
        case NUMBER: return "NUMBER";
        case STAR: return "STAR";
        case COLON: return "COLON";
        case SEMICOLON: return "SEMICOLON";
        case EQUAL: return "EQUAL";
        case PLUS: return "PLUS";
        case MINUS: return "MINUS";
        case GREATERTHAN: return "GREATERTHAN";
        case LESSTHAN: return "LESSTHAN";
        case ASSIGN: return "ASSIGN";
        case WHILE: return "WHILE";
        case IF: return "IF";
        case ELSE: return "ELSE";
        case WRITE: return "WRITE";
        case READ: return "READ";
        case INT: return "INT";
        case EXCLAMATIONMARK: return "EXCLAMATIONMARK";
        case L_BRACE: return "L_BRACE";
        case L_PARANTHESIS: return "L_PARANTHESIS";
        case L_SQUARE: return "L_SQUARE";
        case R_BRACE: return "R_BRACE";
        case R_PARANTHESIS: return "R_PARANTHESIS";
        case R_SQUARE: return "R_SQUARE";
        case DOUBLEAMPERSANT: return "DOUBLEAMPERSANT";
        case EQUALCOLONEQUAL: return "EQUALCOLONEQUAL";
        case END_OF_FILE: return "END_OF_FILE";
        default: return "Invalid tokentype";
    }
}
