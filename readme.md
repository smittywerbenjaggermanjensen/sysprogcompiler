Das Projekt wurde in 5 Teilprojekte aufgeteilt:

    - Automat
    - Buffer
    - Symboltabelle
    - Scanner
    - Parser

Jedes Teilprojekt hat die Verzeichnisstruktur:

	- src			enthaelt den Source-Code
	- includes		enthaelt die Headerfiles
	- objs			enthaelt die Objektfiles, die notwenig fuer das Gesamtprojekt sind
	- debug			enthaelt die Objektfiles und das ausfuehrbare File zum Testen  des Teilprojekts
	


Makefiles:

http://www.gnu.org/software/make/

http://mrbook.org/blog/tutorials/make/

http://de.wikipedia.org/wiki/Make

http://www.sethi.org/classes/cet375/lab_notes/lab_04_makefile_and_compilation.html
