#include "../includes/Parser.h"
#include <fstream>
#include <iostream>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cout << "Usage: inputfile" << std::endl;
        return (EXIT_FAILURE);
    }

    Buffer* buffer = new Buffer(argv[1]);
    Parser* parser = new Parser(buffer);

    parser->parse();
}
