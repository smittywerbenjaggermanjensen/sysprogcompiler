#include "../includes/Parser.h"

Parser::Parser(Buffer *buffer) {
    symtab = new Symboltable();
    scanner = new Scanner(buffer, symtab);
}

Parser::~Parser() {
    delete scanner;
    scanner = 0;
}

void Parser::parse() {
    ParseTree parseTree = ParseTree();
    Node *prog = parseTree.createTree(scanner);

    if (prog) {
        TypeChecker typeChecker = TypeChecker(symtab);
        if (typeChecker.typeCheck(prog)) {
            CodeGenerator codeGenerator = CodeGenerator("prog.code");
            codeGenerator.generateCode(prog);
        }
    }
}