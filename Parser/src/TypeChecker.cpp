#include "../includes/TypeChecker.h"
#include <stdlib.h>


TypeChecker::TypeChecker(Symboltable *symtab) {
    symboltable = symtab;
}

TypeChecker::~TypeChecker() {
}

int TypeChecker::typeCheck(Node *prog) {
    check_PROG(prog);
    std::cout << "TypeCheck finished successfully." << std::endl;
    return 1;
}

///PROG
//PROG -> DECLS STATEMENTS
Node *TypeChecker::check_PROG(Node *root) {
    Node *decls = root->getChild(0);
    Node *statements = root->getChild(1);

    visit(decls);
    visit(statements);

    root->setNodeType(NO_TYPE);

    return root;
}

///DECLS
//DECLS -> DECL ; DECLS
void TypeChecker::check_DECLS(Node *node) {
    Node *decl = node->getChild(0);
    Node *decls = node->getChild(1);

    visit(decl);
    if (decls) {
        visit(decls);
    }

    node->setNodeType(NO_TYPE);
}


///DECL
//DECL -> int ARRAY identifier
void TypeChecker::check_DECL(Node *node) {
    Node *firstChild = node->getChild(0);
    Node *identifier;

    if (firstChild->getRuleType() == RULE_TYPE_ARRAY) {
        identifier = node->getChild(1);
        visit(firstChild);
    } else {
        identifier = firstChild;
    }

    if (getNodeTypeForIdentifier(identifier) != NO_TYPE) {
        error("Identifier already declared!", identifier);
        node->setNodeType(ERROR);

    } else if (firstChild->getNodeType() == ERROR) {
        node->setNodeType(ERROR);

    } else {
        node->setNodeType(NO_TYPE);
        if (firstChild->getNodeType() == ARRAY) {
            setNodeTypeForIdentifier(identifier, INT_ARRAY);
            identifier->setNodeType(INT_ARRAY);
        } else {
            setNodeTypeForIdentifier(identifier, INT);
            identifier->setNodeType(INT);
        }
    }
}


///ARRAY
//ARRAY -> [integer]
void TypeChecker::check_ARRAY(Node *node) {
    Node *integer = node->getChild(0);

    if (integer->getToken()->getTokenType() == Token::NUMBER && integer->getToken()->getNumber() > 0) {
        node->setNodeType(ARRAY);
    } else {
        error("Number missing for ARRAY", node);
        node->setNodeType(ERROR);
    }
}


///STATEMENTS
//STATEMENTS -> STATEMENT; STATEMENTS)
void TypeChecker::check_STATEMENTS(Node *node) {
    Node *statement = node->getChild(0);
    Node *statements = node->getChild(1);

    visit(statement);

    if (statements) {
        visit(statements);
    }

    node->setNodeType(NO_TYPE);
}


///STATEMENT
//STATEMENT -> identifier INDEX := EXP
void TypeChecker::check_STATEMENT_IDENTIFIER(Node *node) {
    Node *exp = NULL;
    Node *index = NULL;
    Node *identifier = node->getChild(0);

    if (node->getChild(2)) {
        exp = node->getChild(2);
        index = node->getChild(1);
        visit(exp);
        visit(index);
    } else {
        //INDEX -> e
        exp = node->getChild(1);
        visit(exp);
    }

    NodeType identifierType = getNodeTypeForIdentifier(identifier);

    if (identifierType == NO_TYPE) {
        error("Identifier is not defined", identifier);
        node->setNodeType(ERROR);

    } else if (exp->getNodeType() == INT && (
            (identifierType == INT && !index)
            || (identifierType == INT_ARRAY && index && index->getNodeType() == ARRAY))) {
        node->setNodeType(NO_TYPE);

    } else {
        error("Types not compatible", identifier);
        node->setNodeType(ERROR);
    }
}

//STATEMENT -> write(EXP)
void TypeChecker::check_STATEMENT_WRITE(Node *node) {
    Node *exp = node->getChild(0);
    visit(exp);
    node->setNodeType(NO_TYPE);
}

//STATEMENT -> read(identifier INDEX)
void TypeChecker::check_STATEMENT_READ(Node *node) {
    Node *identifier = node->getChild(0);
    Node *index = node->getChild(1);

    if (index) {
        visit(index);
    }

    NodeType identifierType = getNodeTypeForIdentifier(identifier);
    if (identifierType == NO_TYPE) {
        error("Identifier not defined", identifier);
        node->setNodeType(ERROR);

    } else if ((identifierType == INT && !index)
               || (identifierType == INT_ARRAY && index && index->getNodeType() == ARRAY)) {
        node->setNodeType(NO_TYPE);

    } else {
        error("Incompatible types in READ statement", identifier);
        node->setNodeType(ERROR);
    }
}

//STATEMENT -> {STATEMENTS}
void TypeChecker::check_STATEMENT_BRACES(Node *node) {
    Node *statements = node->getChild(0);
    if (statements) {
        visit(statements);
    }
    node->setNodeType(NO_TYPE);
}

//STATEMENT -> if (EXP) STATEMENT else STATEMENT
void TypeChecker::check_STATEMENT_IF(Node *node) {
    Node *exp = node->getChild(0);
    Node *statement1 = node->getChild(1);
    Node *statement2 = node->getChild(2);

    visit(exp);
    visit(statement1);
    visit(statement2);

    if (exp->getNodeType() == ERROR) {
        node->setNodeType(ERROR);
    } else {
        node->setNodeType(NO_TYPE);
    }
}

//STATEMENT -> while (EXP) STATEMENT
void TypeChecker::check_STATEMENT_WHILE(Node *node) {
    Node *exp = node->getChild(0);
    Node *statement = node->getChild(1);

    visit(exp);
    visit(statement);

    if (exp->getNodeType() == ERROR) {
        node->setNodeType(ERROR);
    } else {
        node->setNodeType(NO_TYPE);
    }
}


///EXP
//EXP -> EXP2 OP_EXP
void TypeChecker::check_EXP(Node *node) {
    Node *exp2 = node->getChild(0);
    Node *op_exp = node->getChild(1);

    visit(exp2);
    if (op_exp) {
        visit(op_exp);
    }

    if (op_exp && (exp2->getNodeType() != op_exp->getNodeType())) {
        node->setNodeType(ERROR);
    } else {
        node->setNodeType(exp2->getNodeType());
    }
}


///EXP2
//EXP2 -> [ EXP ]
void TypeChecker::check_EXP2_BRACES(Node *node) {
    Node *exp = node->getChild(0);
    visit(exp);
    node->setNodeType(exp->getNodeType());
}

//EXP2 -> identifier INDEX
void TypeChecker::check_EXP2_IDENTIFIER(Node *node) {
    Node *identifier = node->getChild(0);
    Node *index = node->getChild(1);

    if (index) {
        visit(index);
    }

    NodeType identifierType = getNodeTypeForIdentifier(identifier);
    if (identifierType == NO_TYPE) {
        error("Identifier not defined", identifier);
        node->setNodeType(ERROR);
    } else if (identifierType == INT
               && !index) {
        node->setNodeType(INT);
    } else if (identifierType == INT_ARRAY
               && index && index->getNodeType() == ARRAY) {
        node->setNodeType(INT);
    } else {
        error("Not a primitive type", node);
        node->setNodeType(ERROR);
    }
}

//EXP2 -> integer
void TypeChecker::check_EXP2_INTEGER(Node *node) {
    node->setNodeType(INT);
}

//EXP2 -> -EXP2
void TypeChecker::check_EXP2_NEGATIVE(Node *node) {
    Node *exp2 = node->getChild(0);
    visit(exp2);
    node->setNodeType(exp2->getNodeType());
}

//EXP2 -> !EXP2
void TypeChecker::check_EXP2_NEGATION(Node *node) {
    Node *exp2 = node->getChild(0);

    visit(exp2);

    if (exp2->getNodeType() != INT) {
        node->setNodeType(ERROR);
    } else {
        node->setNodeType(INT);
    }
}


///INDEX
//INDEX -> [ EXP ]
void TypeChecker::check_INDEX(Node *node) {
    Node *exp = node->getChild(0);

    visit(exp);

    if (exp->getNodeType() == ERROR) {
        node->setNodeType(ERROR);
    } else {
        node->setNodeType(ARRAY);
    }
}


///OP_EXP
//OP_EXP -> OP EXP
void TypeChecker::check_OPEXP(Node *node) {
    Node *op = node->getChild(0);
    Node *exp = node->getChild(1);

    visit(op);
    visit(exp);

    node->setNodeType(exp->getNodeType());
}


///OP
//OP -> +
void TypeChecker::check_OP_PLUS(Node *node) {
    node->setNodeType(OP_PLUS);
}

//OP -> -
void TypeChecker::check_OP_MINUS(Node *node) {
    node->setNodeType(OP_MINUS);
}

//OP -> *
void TypeChecker::check_OP_MULTIPLICATION(Node *node) {
    node->setNodeType(OP_MULTIPLICATION);
}

//OP -> :
void TypeChecker::check_OP_DIVISION(Node *node) {
    node->setNodeType(OP_DIVISION);
}

//OP -> <
void TypeChecker::check_OP_LESS(Node *node) {
    node->setNodeType(OP_LESSTHAN);
}

//OP -> >
void TypeChecker::check_OP_GREATER(Node *node) {
    node->setNodeType(OP_GREATERTHAN);
}

//OP -> =
void TypeChecker::check_OP_EQUAL(Node *node) {
    node->setNodeType(OP_EQUAL);
}

//OP -> =:=
void TypeChecker::check_OP_EQUALCOLONEQUAL(Node *node) {
    node->setNodeType(OP_UNEQUAL);
}

//OP -> &&
void TypeChecker::check_OP_AND(Node *node) {
    node->setNodeType(OP_AND);
}


void TypeChecker::error(std::string message, Node *node) {
    if (node) {
        Token *token = node->getToken();
        std::cerr << "TypeCheck SEMANTIC ERROR: " << message << " in row: "
                  << token->getRow() << " column: " << token->getColumn() << std::endl;
    } else {
        std::cerr << "TypeCheck SEMANTIC ERROR: " << message << std::endl;
    }
    exit(1);
}

void TypeChecker::visit(Node *node) {

    if (!node) {
        error("Node is NULL", node);
        return;
    }

    switch (node->getRuleType()) {
        case RULE_TYPE_PROG:
            check_PROG(node);
            break;

        case RULE_TYPE_DECLS:
            check_DECLS(node);
            break;

        case RULE_TYPE_DECL:
            check_DECL(node);
            break;

        case RULE_TYPE_ARRAY:
            check_ARRAY(node);
            break;

        case RULE_TYPE_STATEMENTS:
            check_STATEMENTS(node);
            break;

        case RULE_TYPE_STATEMENT_IDENTIFIER:
            check_STATEMENT_IDENTIFIER(node);
            break;

        case RULE_TYPE_STATEMENT_WRITE:
            check_STATEMENT_WRITE(node);
            break;

        case RULE_TYPE_STATEMENT_READ:
            check_STATEMENT_READ(node);
            break;

        case RULE_TYPE_STATEMENT_BRACES:
            check_STATEMENT_BRACES(node);
            break;

        case RULE_TYPE_STATEMENT_IF:
            check_STATEMENT_IF(node);
            break;

        case RULE_TYPE_STATEMENT_WHILE:
            check_STATEMENT_WHILE(node);
            break;

        case RULE_TYPE_EXP:
            check_EXP(node);
            break;

        case RULE_TYPE_EXP2_BRACES:
            check_EXP2_BRACES(node);
            break;

        case RULE_TYPE_EXP2_IDENTIFIER:
            check_EXP2_IDENTIFIER(node);
            break;

        case RULE_TYPE_EXP2_INTEGER:
            check_EXP2_INTEGER(node);
            break;

        case RULE_TYPE_EXP2_NEGATIVE:
            check_EXP2_NEGATIVE(node);
            break;

        case RULE_TYPE_EXP2_NEGATION:
            check_EXP2_NEGATION(node);
            break;

        case RULE_TYPE_INDEX:
            check_INDEX(node);
            break;

        case RULE_TYPE_OP_EXP:
            check_OPEXP(node);
            break;

        case RULE_TYPE_OP:
            switch (node->getToken()->getTokenType()) {
                case Token::PLUS:
                    check_OP_PLUS(node);
                    break;
                case Token::MINUS:
                    check_OP_MINUS(node);
                    break;
                case Token::STAR:
                    check_OP_MULTIPLICATION(node);
                    break;
                case Token::COLON:
                    check_OP_DIVISION(node);
                    break;
                case Token::LESSTHAN:
                    check_OP_LESS(node);
                    break;
                case Token::GREATERTHAN:
                    check_OP_GREATER(node);
                    break;
                case Token::EQUAL:
                    check_OP_EQUAL(node);
                    break;
                case Token::EQUALCOLONEQUAL:
                    check_OP_EQUALCOLONEQUAL(node);
                    break;
                case Token::DOUBLEAMPERSANT:
                    check_OP_AND(node);
                    break;
                default:
                    error("Not a valid OPERATOR", node);
                    break;
            }
            break;

        default:
            error("Not a valid RULETYPE", node);
    }
}

NodeType TypeChecker::getNodeTypeForIdentifier(Node *identifier) {
    if (identifier->isLeaf()) {
        Information *info = symboltable->lookup(identifier->getToken()->getSymbolTableKey());
        switch (info->getIdentifierType()) {
            case 0:
                return NO_TYPE;
            case 1:
                return INT;
            case 2:
                return INT_ARRAY;
            default:
                break;
        }
    }
    //should not happen
    return ERROR;
}

void TypeChecker::setNodeTypeForIdentifier(Node *identifier, NodeType nodeType) {
    if (identifier->isLeaf()) {
        Information *info = symboltable->lookup(identifier->getToken()->getSymbolTableKey());
        switch (nodeType) {
            case NO_TYPE:
                info->setIdentifierType(0);
                break;
            case INT:
                info->setIdentifierType(1);
                break;
            case INT_ARRAY:
                info->setIdentifierType(2);
                break;
            default:
                //should not happen
                break;
        }
    }
}
