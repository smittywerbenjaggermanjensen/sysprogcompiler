//
// Created by Henning on 01.04.2019.
//

#include "../includes/Node.h"

Node::Node(RuleType_t ruleType) {
    this->ruleType = ruleType;
    this->childrenCount = 0;
    this->nodeType = NO_TYPE;
}

void Node::addChild(Node *child) {
    children[childrenCount++] = child;
}

Node *Node::getChild(int index) {
    return children[index];
}

void Node::setParent(Node *parent) {
    this->parent = parent;
}

Node *Node::getParent(void) {
    return parent;
}

bool Node::isLeaf(void) {
    return childrenCount == 0;
}

RuleType_t Node::getRuleType(void) {
    return ruleType;
}

void Node::setToken(Token *token) {
    this->token = token;
}

Token *Node::getToken(void) {
    return token;
}

void Node::setNodeType(NodeType nodeType) {
    this->nodeType = nodeType;

}

NodeType Node::getNodeType(void) {
    return this->nodeType;
}
