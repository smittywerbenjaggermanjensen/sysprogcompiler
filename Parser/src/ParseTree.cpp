//
// Created by Henning on 01.04.2019.
//

#include "../includes/RuleType.h"
#include "../includes/ParseTree.h"

#include <iostream>
#include <stdexcept>
#include <stdlib.h>

ParseTree::ParseTree() {
}

Node* ParseTree::createTree(Scanner* scanner) {
    /* Start with ParseTree-Node as root, first and only child is PROG-Node */
    this->scanner = scanner;
    currentToken = this->scanner->nextToken();

    try {
        Node* progNode = doPROG(NULL);
        std::cout << "ParseTree finished successfully." << std::endl;
        return progNode;
    }
    catch (const std::invalid_argument& e) {
        std::cerr << e.what() << std::endl;
    }
    catch (const std::exception ex) {
        std::cerr << "Unknown error:" << std::endl;
        std::cerr << ex.what() << std::endl;
    }

    return NULL;
}

Node* ParseTree::doPROG(Node* parent) {     /* Parent is root */
    Node* progNode = addNode(RULE_TYPE_PROG, currentToken, parent);

    doDECLS(progNode);
    doSTATEMENTS(progNode);
    expect(Token::END_OF_FILE);
    return progNode;
}

void ParseTree::doDECLS(Node* parent) {                 /* Parent is PROG or DECLS */
    if (currentToken->getTokenType() == Token::INT) {   /* e check */
        Node *declsNode = addNode(RULE_TYPE_DECLS, currentToken, parent);

        doDECL(declsNode);
        expect(Token::SEMICOLON);
        doDECLS(declsNode);
    }
}

void ParseTree::doDECL(Node* parent) {      /* Parent is DECLS */
    Node* declNode = addNode(RULE_TYPE_DECL, currentToken, parent);

    expect(Token::INT);
    doARRAY(declNode);
    doIDENTIFIER(declNode);
}

void ParseTree::doARRAY(Node* parent) {                     /* Parent is DECL */
    if (currentToken->getTokenType() == Token::L_SQUARE) {  /* e check */
        Node *arrayNode = addNode(RULE_TYPE_ARRAY, currentToken, parent);

        expect(Token::L_SQUARE);
        doINTEGER(arrayNode);
        expect(Token::R_SQUARE);
    }
}

void ParseTree::doSTATEMENTS(Node* parent) {    /* Parent is PROG, STATEMENTS or STATEMENT */
    Node* statementsNode;
    switch (currentToken->getTokenType()) {     /* e check */
        case Token::IDENTIFIER:
        case Token::WRITE:
        case Token::READ:
        case Token::L_BRACE:
        case Token::IF:
        case Token::WHILE:
            statementsNode = addNode(RULE_TYPE_STATEMENTS, currentToken, parent);

            doSTATEMENT(statementsNode);
            expect(Token::SEMICOLON);
            doSTATEMENTS(statementsNode);
            break;

        default:
            return;
    }
}

void ParseTree::doSTATEMENT(Node* parent) {     /* Parent is STATEMENTS or STATEMENT */
    Node* statementNode;
    switch(currentToken->getTokenType()) {
        case Token::IDENTIFIER:
            statementNode = addNode(RULE_TYPE_STATEMENT_IDENTIFIER, currentToken, parent);
            doIDENTIFIER(statementNode);
            doINDEX(statementNode);
            expect(Token::ASSIGN);
            doEXP(statementNode);
            break;

        case Token::WRITE:
            statementNode = addNode(RULE_TYPE_STATEMENT_WRITE, currentToken, parent);
            expect(Token::WRITE);
            expect(Token::L_PARANTHESIS);
            doEXP(statementNode);
            expect(Token::R_PARANTHESIS);
            break;

        case Token::READ:
            statementNode = addNode(RULE_TYPE_STATEMENT_READ, currentToken, parent);
            expect(Token::READ);
            expect(Token::L_PARANTHESIS);
            doIDENTIFIER(statementNode);
            doINDEX(statementNode);
            expect(Token::R_PARANTHESIS);
            break;

        case Token::L_BRACE:
            statementNode = addNode(RULE_TYPE_STATEMENT_BRACES, currentToken, parent);
            expect(Token::L_BRACE);
            doSTATEMENTS(statementNode);
            expect(Token::R_BRACE);
            break;

        case Token::IF:
            statementNode = addNode(RULE_TYPE_STATEMENT_IF, currentToken, parent);
            expect(Token::IF);
            expect(Token::L_PARANTHESIS);
            doEXP(statementNode);
            expect(Token::R_PARANTHESIS);
            doSTATEMENT(statementNode);
            expect(Token::ELSE);
            doSTATEMENT(statementNode);
            break;

        case Token::WHILE:
            statementNode = addNode(RULE_TYPE_STATEMENT_WHILE, currentToken, parent);
            expect(Token::WHILE);
            expect(Token::L_PARANTHESIS);
            doEXP(statementNode);
            expect(Token::R_PARANTHESIS);
            doSTATEMENT(statementNode);
            break;

        default:
            throwError("STATEMENT");
            break;
    }
}

void ParseTree::doEXP(Node* parent) {
    Node* expNode = addNode(RULE_TYPE_EXP, currentToken, parent);

    doEXP2(expNode);
    doOP_EXP(expNode);
}

void ParseTree::doEXP2(Node* parent) {          /* Parent is EXP or EXP2 */
    Node* exp2Node;

    switch(currentToken->getTokenType()) {
        case Token::L_PARANTHESIS:
            exp2Node = addNode(RULE_TYPE_EXP2_BRACES, currentToken, parent);
            expect(Token::L_PARANTHESIS);
            doEXP(exp2Node);
            expect(Token::R_PARANTHESIS);
            break;

        case Token::IDENTIFIER:
            exp2Node = addNode(RULE_TYPE_EXP2_IDENTIFIER, currentToken, parent);
            doIDENTIFIER(exp2Node);
            doINDEX(exp2Node);
            break;

        case Token::NUMBER:
            exp2Node = addNode(RULE_TYPE_EXP2_INTEGER, currentToken, parent);
            doINTEGER(exp2Node);
            break;

        case Token::MINUS:
            exp2Node = addNode(RULE_TYPE_EXP2_NEGATIVE, currentToken, parent);
            expect(Token::MINUS);
            doEXP2(exp2Node);
            break;

        case Token::EXCLAMATIONMARK:
            exp2Node = addNode(RULE_TYPE_EXP2_NEGATION, currentToken, parent);
            expect(Token::EXCLAMATIONMARK);
            doEXP2(exp2Node);
            break;

        default:
            throwError("EXP2");
            break;
    }
}

void ParseTree::doINDEX(Node* parent) {                     /* Parent is STATEMENT or EXP2 */
    if (currentToken->getTokenType() == Token::L_SQUARE) {  /* e check */
        Node *indexNode = addNode(RULE_TYPE_INDEX, currentToken, parent);

        expect(Token::L_SQUARE);
        doEXP(indexNode);
        expect(Token::R_SQUARE);
    }
}

void ParseTree::doOP_EXP(Node* parent) {    /* parent is EXP */
    if (isTokenOperand(currentToken)) {     /* e check */
        Node *opexpNode = addNode(RULE_TYPE_OP_EXP, currentToken, parent);

        doOP(opexpNode);
        doEXP(opexpNode);
    }
}

void ParseTree::doOP(Node* parent) {   /* parent is OP_EXP */
    addNode(RULE_TYPE_OP, currentToken, parent);

    if (isTokenOperand(currentToken)) {     /* Do we need this check? Already checked in OP_EXP which is the only caller */
        currentToken = scanner->nextToken();    /* TODO: cleanup */
    }
    else {
        throwError("operand");  /* Expected an operand */
    }
}

void ParseTree::doIDENTIFIER(Node* parent) {        /* Parent is DECL, STATEMENT or EXP2 */
    addNode(RULE_TYPE_IDENTIFIER, currentToken, parent);
    expect(Token::IDENTIFIER);
}

void ParseTree::doINTEGER(Node* parent) {           /* Parent is ARRAY or EXP2 */
    addNode(RULE_TYPE_INTEGER, currentToken, parent);
    expect(Token::NUMBER);
}

int ParseTree::expect(Token::TokenType tokenType) {
    if (currentToken->getTokenType() == tokenType) {
        currentToken = scanner->nextToken();

        if (currentToken->getTokenType() == Token::ERROR) {
        	exit(1); // Error message is printed in scanner, because error comes from scanner.
        }
        return 1;
    }
    throwError(tokenType);

//    std::cerr << "Error: Expected " << currentToken->getTokenTypeString(tokenType) << " but got TokenType " << currentToken->getTokenTypeString() << " in R:" << currentToken->getRow() << " C:" << currentToken->getColumn() << std::endl;
    return 0;
}

Node* ParseTree::addNode(RuleType_t ruleType, Token* token, Node* parent) {
    Node* newNode = new Node(ruleType);
    newNode->setToken(token);
    newNode->setParent(parent);
    if (parent) {
        parent->addChild(newNode);
    }

    return newNode;
}

int ParseTree::isTokenOperand(Token* token) {
    switch (token->getTokenType()) {
        case Token::PLUS:
        case Token::MINUS:
        case Token::STAR:
        case Token::COLON:
        case Token::LESSTHAN:
        case Token::GREATERTHAN:
        case Token::EQUAL:
        case Token::EQUALCOLONEQUAL:
        case Token::DOUBLEAMPERSANT:
            return 1;

        default:
            return 0;
    }
}

void ParseTree::throwError(Token::TokenType expectedTokenType) {
    std::string output;

    output += "Error: Expected ";
    output += currentToken->getTokenTypeString(expectedTokenType);
    output += " but got TokenType ";
    output += currentToken->getTokenTypeString();

    if (currentToken->getTokenType() != Token::END_OF_FILE) {
        //output += " in R:";
        //output += std::to_string(currentToken->getRow());
        //output += " C:";
        //output += std::to_string(currentToken->getColumn());
    }

    throw std::invalid_argument(output);
}

void ParseTree::throwError(std::string msg) {
    std::string output;

    output += "Error: Expected ";
    output += msg;
    output += " but got TokenType ";
    output += currentToken->getTokenTypeString();
    //output += " in R:";
    //output += std::to_string(currentToken->getRow());
    //output += " C:";
    //output += std::to_string(currentToken->getColumn());

    throw std::invalid_argument(output);
}

std::string ParseTree::getRuleTypeString(RuleType_t ruleType) {
    switch (ruleType) {
        case RULE_TYPE_PROG: return "RULE_TYPE_PROG";
        case RULE_TYPE_DECLS: return "RULE_TYPE_DECLS";
        case RULE_TYPE_DECL: return "RULE_TYPE_DECL";
        case RULE_TYPE_ARRAY: return "RULE_TYPE_ARRAY";
        case RULE_TYPE_STATEMENTS: return "RULE_TYPE_STATEMENTS";
        case RULE_TYPE_STATEMENT_IDENTIFIER: return "RULE_TYPE_STATEMENT_IDENTIFIER";
        case RULE_TYPE_STATEMENT_WRITE: return "RULE_TYPE_STATEMENT_WRITE";
        case RULE_TYPE_STATEMENT_READ: return "RULE_TYPE_STATEMENT_READ";
        case RULE_TYPE_STATEMENT_BRACES: return "RULE_TYPE_STATEMENT_BRACES";
        case RULE_TYPE_STATEMENT_IF: return "RULE_TYPE_STATEMENT_IF";
        case RULE_TYPE_STATEMENT_WHILE: return "RULE_TYPE_STATEMENT_WHILE";
        case RULE_TYPE_EXP: return "RULE_TYPE_EXP";
        case RULE_TYPE_EXP2_BRACES: return "RULE_TYPE_EXP2_BRACES";
        case RULE_TYPE_EXP2_IDENTIFIER: return "RULE_TYPE_EXP2_IDENTIFIER";
        case RULE_TYPE_EXP2_INTEGER: return "RULE_TYPE_EXP2_INTEGER";
        case RULE_TYPE_EXP2_NEGATIVE: return "RULE_TYPE_EXP2_NEGATIVE";
        case RULE_TYPE_EXP2_NEGATION: return "RULE_TYPE_EXP2_NEGATION";
        case RULE_TYPE_INDEX: return "RULE_TYPE_INDEX";
        case RULE_TYPE_OP_EXP: return "RULE_TYPE_OP_EXP";
        case RULE_TYPE_OP: return "RULE_TYPE_OP";
        case RULE_TYPE_IDENTIFIER: return "RULE_TYPE_IDENTIFIER";
        case RULE_TYPE_INTEGER: return "RULE_TYPE_INTEGER";
        case RULE_TYPE_ROOT: return "RULE_TYPE_ROOT";
        case RULE_TYPE_UNKNOWN: return "RULE_TYPE_UNKNOWN";
        default: return "invalid RuleType";
    }
}
