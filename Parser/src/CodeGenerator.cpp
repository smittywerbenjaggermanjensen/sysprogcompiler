#include "../includes/CodeGenerator.h"
#include <stdlib.h>


CodeGenerator::CodeGenerator(const char *outfile) {
    codeFile = new std::ofstream(outfile, std::ios::trunc);
    labelCounter = 0;
}

CodeGenerator::~CodeGenerator() {
    codeFile->close();
    delete codeFile;
}

void CodeGenerator::generateCode(Node *prog) {
    generate_PROG(prog);
    codeFile->flush();
    std::cout << "CodeGeneration finished successfully." << std::endl;
}

//PROG -> DECLS STATEMENTS
void CodeGenerator::generate_PROG(Node *root) {
    Node *decls = root->getChild(0);
    Node *statements = root->getChild(1);

    visit(decls);
    visit(statements);

    *codeFile << "STP";
}

//DECLS -> DECL; DECLS
void CodeGenerator::generate_DECLS(Node *node) {
    Node *decl = node->getChild(0);
    Node *decls = node->getChild(1);

    visit(decl);
    if (decls) {
        visit(decls);
    }
}


//DECL -> int ARRAY identifier
void CodeGenerator::generate_DECL(Node *node) {
    Node *identifier;
    Node *array = NULL;

    if (node->getChild(0)->getRuleType() == RULE_TYPE_ARRAY) {
        identifier = node->getChild(1);
        array = node->getChild(0);
    } else {
        identifier = node->getChild(0);
    }

    *codeFile << "DS $" << identifier->getToken()->getLexem();

    if (array) {
        visit(array);
    } else {
        *codeFile << " " << 1 << " ";
    }
}

//ARRAY -> [integer]
void CodeGenerator::generate_ARRAY(Node *node) {
    Node *array = node->getChild(0);
    *codeFile << " " << array->getToken()->getNumber() << " ";
}


//STATEMENTS -> STATEMENT; STATEMENTS)
void CodeGenerator::generate_STATEMENTS(Node *node) {
    Node *statement = node->getChild(0);
    Node *statements = node->getChild(1);

    visit(statement);

    if (statements) {
        visit(statements);
    }
}


//STATEMENT -> identifier INDEX := EXP
void CodeGenerator::generate_STATEMENT_IDENTIFIER(Node *node) {
    Node *exp = NULL;
    Node *index = NULL;
    Node *identifier = node->getChild(0);

    if (node->getChild(2)) {
        exp = node->getChild(2);
        index = node->getChild(1);
    } else {
        //INDEX -> e
        exp = node->getChild(1);
    }

    visit(exp);

    *codeFile << " LA " << "$" << identifier->getToken()->getLexem();
    if (index) {
        visit(index);
    }

    *codeFile << " STR ";
}

//STATEMENT -> write(EXP)
void CodeGenerator::generate_STATEMENT_WRITE(Node *node) {
    Node *exp = node->getChild(0);
    visit(exp);

    *codeFile << " PRI ";
}

//STATEMENT -> read(identifier INDEX)
void CodeGenerator::generate_STATEMENT_READ(Node *node) {
    Node *identifier = node->getChild(0);
    Node *index = node->getChild(1);


    *codeFile << " REA ";
    *codeFile << " LA " << "$" << identifier->getToken()->getLexem();

    if (index) {
        visit(index);
    }

    *codeFile << " STR ";
}

//STATEMENT -> { STATEMENTS }
void CodeGenerator::generate_STATEMENT_BRACES(Node *node) {
    Node *statements = node->getChild(0);
    if (statements) {
        visit(statements);
    }
}

//STATEMENT -> if (EXP) STATEMENT else STATEMENT
void CodeGenerator::generate_STATEMENT_IF(Node *node) {
    Node *condition = node->getChild(0);
    Node *ifStatement = node->getChild(1);
    Node *elseStatement = node->getChild(2);
    int label1 = labelCounter++;
    int label2 = labelCounter++;

    visit(condition);
    *codeFile << " JIN " << "#label" << label1 << " ";

    visit(ifStatement);
    *codeFile << " JMP " << "#label" << label2 << " ";
    *codeFile << "#label" << label1 << " NOP ";

    visit(elseStatement);
    *codeFile << "#label" << label2 << " NOP ";
}

//STATEMENT -> while ( EXP ) STATEMENT
void CodeGenerator::generate_STATEMENT_WHILE(Node *node) {
    Node *exp = node->getChild(0);
    Node *statement = node->getChild(1);

    int label1 = labelCounter++;
    int label2 = labelCounter++;

    *codeFile << "#label" << label1 << " NOP ";

    visit(exp);

    *codeFile << " JIN " << "#label" << label2 << " ";

    visit(statement);

    *codeFile << " JMP " << "#label" << label1 << " ";
    *codeFile << "#label" << label2 << " NOP ";
}

//EXP -> EXP2_BRACES OP_EXP
void CodeGenerator::generate_EXP(Node *node) {
    Node *exp2 = node->getChild(0);
    Node *op_exp = node->getChild(1);

    if (op_exp && op_exp->getNodeType() == NO_TYPE) {
        visit(exp2);

    } else if (op_exp && op_exp->getChild(0)->getNodeType() == OP_GREATERTHAN) {
        visit(op_exp);
        visit(exp2);
        *codeFile << " LES ";

    } else if (op_exp && op_exp->getChild(0)->getNodeType() == OP_UNEQUAL) {
        visit(exp2);
        visit(op_exp);
        *codeFile << " NOT ";

    } else {
        visit(exp2);
        if (op_exp) {
            visit(op_exp);
        }
    }
}

//INDEX -> [ EXP ]
void CodeGenerator::generate_EXP2_BRACES(Node *node) {
    Node *exp = node->getChild(0);
    visit(exp);
}

//EXP2_BRACES -> identifier INDEX
void CodeGenerator::generate_EXP2_IDENTIFIER(Node *node) {
    Node *identifier = node->getChild(0);
    Node *index = node->getChild(1);

    *codeFile << " LA $" << identifier->getToken()->getLexem();

    if (index) {
        visit(index);
    }

    *codeFile << " LV ";
}

//EXP2_BRACES -> integer
void CodeGenerator::generate_EXP2_INTEGER(Node *node) {
    *codeFile << " LC " << node->getChild(0)->getToken()->getNumber();
}

//EXP2_BRACES -> - EXP2_BRACES
void CodeGenerator::generate_EXP2_NEGATIVE(Node *node) {
    Node *exp2 = node->getChild(0);

    *codeFile << "LC " << 0;
    visit(exp2);
    *codeFile << " SUB ";
}

//EXP2_BRACES -> ! EXP2_BRACES
void CodeGenerator::generate_EXP2_NEGATION(Node *node) {
    Node *exp2 = node->getChild(0);

    visit(exp2);
    *codeFile << " NOT ";
}

//INDEX -> [ EXP ]
void CodeGenerator::generate_INDEX(Node *node) {
    Node *exp = node->getChild(0);

    visit(exp);
    *codeFile << " ADD ";
}


//OP_EXP -> OP_PLUS EXP
void CodeGenerator::generate_OP_EXP(Node *node) {
    Node *op = node->getChild(0);
    Node *exp = node->getChild(1);

    visit(exp);
    visit(op);
}


void CodeGenerator::generate_OP_PLUS() {
    *codeFile << " ADD ";
}


void CodeGenerator::generate_OP_MINUS() {
    *codeFile << " SUB ";
}

void CodeGenerator::generate_OP_MULTIPLICATION() {
    *codeFile << " MUL ";
}

void CodeGenerator::generate_OP_DIVISION() {
    *codeFile << " DIV ";
}

void CodeGenerator::generate_OP_LESS() {
    *codeFile << " LES ";
}

void CodeGenerator::generate_OP_GREATER() {
    *codeFile << "  ";
}

void CodeGenerator::generate_OP_EQUALS() {
    *codeFile << " EQU ";

}

void CodeGenerator::generate_OP_EQUALCOLONEQUAL() {
    *codeFile << " EQU ";
}

void CodeGenerator::generate_OP_AND() {
    *codeFile << " AND ";
}

void CodeGenerator::visit(Node *node) {
    if (!node) {
        std::cerr << "Node is NULL" << std::endl;
        return;
    }

    switch (node->getRuleType()) {
        case RULE_TYPE_PROG:
            generate_PROG(node);
            break;
        case RULE_TYPE_DECLS:
            generate_DECLS(node);
            break;
        case RULE_TYPE_DECL:
            generate_DECL(node);
            break;
        case RULE_TYPE_ARRAY:
            generate_ARRAY(node);
            break;
        case RULE_TYPE_STATEMENTS:
            generate_STATEMENTS(node);
            break;
        case RULE_TYPE_STATEMENT_IDENTIFIER:
            generate_STATEMENT_IDENTIFIER(node);
            break;
        case RULE_TYPE_STATEMENT_WRITE:
            generate_STATEMENT_WRITE(node);
            break;
        case RULE_TYPE_STATEMENT_READ:
            generate_STATEMENT_READ(node);
            break;
        case RULE_TYPE_STATEMENT_BRACES:
            generate_STATEMENT_BRACES(node);
            break;
        case RULE_TYPE_STATEMENT_IF:
            generate_STATEMENT_IF(node);
            break;
        case RULE_TYPE_STATEMENT_WHILE:
            generate_STATEMENT_WHILE(node);
            break;
        case RULE_TYPE_EXP:
            generate_EXP(node);
            break;
        case RULE_TYPE_EXP2_BRACES:
            generate_EXP2_BRACES(node);
            break;
        case RULE_TYPE_EXP2_IDENTIFIER:
            generate_EXP2_IDENTIFIER(node);
            break;
        case RULE_TYPE_EXP2_INTEGER:
            generate_EXP2_INTEGER(node);
            break;
        case RULE_TYPE_EXP2_NEGATIVE:
            generate_EXP2_NEGATIVE(node);
            break;
        case RULE_TYPE_EXP2_NEGATION:
            generate_EXP2_NEGATION(node);
            break;
        case RULE_TYPE_INDEX:
            generate_INDEX(node);
            break;
        case RULE_TYPE_OP_EXP:
            generate_OP_EXP(node);
            break;

        case RULE_TYPE_OP:
            switch (node->getToken()->getTokenType()) {
                case Token::PLUS:
                    generate_OP_PLUS();
                    break;
                case Token::MINUS:
                    generate_OP_MINUS();
                    break;
                case Token::STAR:
                    generate_OP_MULTIPLICATION();
                    break;
                case Token::COLON:
                    generate_OP_DIVISION();
                    break;
                case Token::LESSTHAN:
                    generate_OP_LESS();
                    break;
                case Token::GREATERTHAN:
                    generate_OP_GREATER();
                    break;
                case Token::EQUAL:
                    generate_OP_EQUALS();
                    break;
                case Token::EQUALCOLONEQUAL:
                    generate_OP_EQUALCOLONEQUAL();
                    break;
                case Token::DOUBLEAMPERSANT:
                    generate_OP_AND();
                    break;
                default:
                    break;
            }
            break;

        default:
            //should not happen, already checked by TypeChecker
            std::cerr << "Not a valid RULETYPE" << std::endl;
            exit(1);
    }
}
