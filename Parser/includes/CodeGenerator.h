#ifndef SYSPROGCOMPILER_CODEGENERATOR_H
#define SYSPROGCOMPILER_CODEGENERATOR_H

#include "Node.h"
#include <iostream>
#include <fstream>

class CodeGenerator {
public:
    CodeGenerator(const char* outfile);
    virtual ~CodeGenerator();

    void generateCode(Node *prog);

private:

    std::ofstream* codeFile;

    int labelCounter;

    void visit(Node *node);

    void generate_PROG(Node *root);
    void generate_DECLS(Node *node);
    void generate_DECL(Node *node);
    void generate_ARRAY(Node *node);
    void generate_STATEMENTS(Node *node);
    void generate_STATEMENT_IDENTIFIER(Node *node);
    void generate_STATEMENT_WRITE(Node *node);
    void generate_STATEMENT_READ(Node *node);
    void generate_STATEMENT_BRACES(Node *node);
    void generate_STATEMENT_IF(Node *node);
    void generate_STATEMENT_WHILE(Node *node);
    void generate_EXP(Node *node);
    void generate_EXP2_BRACES(Node *node);
    void generate_EXP2_IDENTIFIER(Node *node);
    void generate_EXP2_INTEGER(Node *node);
    void generate_EXP2_NEGATIVE(Node *node);
    void generate_EXP2_NEGATION(Node *node);
    void generate_INDEX(Node *node);
    void generate_OP_EXP(Node *node);
    void generate_OP_PLUS();
    void generate_OP_MINUS();
    void generate_OP_MULTIPLICATION();
    void generate_OP_DIVISION();
    void generate_OP_LESS();
    void generate_OP_GREATER();
    void generate_OP_EQUALS();
    void generate_OP_EQUALCOLONEQUAL();
    void generate_OP_AND();
};

#endif //SYSPROGCOMPILER_CODEGENERATOR_H