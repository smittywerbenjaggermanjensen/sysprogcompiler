#ifndef SYSPROGCOMPILER_TYPECHECKER_H
#define SYSPROGCOMPILER_TYPECHECKER_H

#include "Node.h"
#include "ParseTree.h"
#include "../../Automat/includes/Token.h"

class TypeChecker {
public:
    TypeChecker(Symboltable *symboltable);
    virtual ~TypeChecker();

    int typeCheck(Node *prog);

private:
    Symboltable *symboltable;

    void visit(Node *node);
    void error(std::string message, Node *node);

    NodeType getNodeTypeForIdentifier(Node *identifier);
    void setNodeTypeForIdentifier(Node *identifier, NodeType nodeType);

    //PROG
    Node* check_PROG(Node *root);

    //DECLS
    void check_DECLS(Node *node);

    //DECL
    void check_DECL(Node *node);

    //ARRAY
    void check_ARRAY(Node *node);

    //STATEMENTS
    void check_STATEMENTS(Node *node);

    //STATEMENT
    void check_STATEMENT_IDENTIFIER(Node *node);
    void check_STATEMENT_WRITE(Node *node);
    void check_STATEMENT_READ(Node *node);
    void check_STATEMENT_BRACES(Node *node);
    void check_STATEMENT_IF(Node *node);
    void check_STATEMENT_WHILE(Node *node);

    //EXP
    void check_EXP(Node *node);

    //EXP2
    void check_EXP2_BRACES(Node *node);
    void check_EXP2_IDENTIFIER(Node *node);
    void check_EXP2_INTEGER(Node *node);
    void check_EXP2_NEGATIVE(Node *node);
    void check_EXP2_NEGATION(Node *node);

    //INDEX
    void check_INDEX(Node *node);

    //OP_EXP
    void check_OPEXP(Node *node);

    //OP
    void check_OP_PLUS(Node *node);
    void check_OP_MINUS(Node *node);
    void check_OP_MULTIPLICATION(Node *node);
    void check_OP_DIVISION(Node *node);
    void check_OP_LESS(Node *node);
    void check_OP_GREATER(Node *node);
    void check_OP_EQUAL(Node *node);
    void check_OP_EQUALCOLONEQUAL(Node *node);
    void check_OP_AND(Node *node);
};


#endif //SYSPROGCOMPILER_TYPECHECKER_H