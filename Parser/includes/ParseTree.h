//
// Created by Henning on 01.04.2019.
//

#ifndef SYSPROGCOMPILER_PARSETREE_H
#define SYSPROGCOMPILER_PARSETREE_H

#include "Node.h"
#include "../../Scanner/includes/Scanner.h"
#include "../../Automat/includes/Token.h"

class ParseTree {
public:
    ParseTree(void);
    Node * createTree(Scanner* scanner);

private:
    Scanner* scanner;
    Token* currentToken;

    Node * doPROG(Node* parent);
    void doDECLS(Node* parent);
    void doDECL(Node* parent);
    void doARRAY(Node* parent);
    void doSTATEMENTS(Node* parent);
    void doSTATEMENT(Node* parent);
    void doEXP(Node* parent);
    void doEXP2(Node* parent);
    void doINDEX(Node* parent);
    void doOP_EXP(Node* parent);
    void doOP(Node* parent);
    void doIDENTIFIER(Node* parent);
    void doINTEGER(Node* parent);

    int expect(Token::TokenType tokenType);

    Node* addNode(RuleType_t ruleType, Token* token, Node* parent);

    int isTokenOperand(Token* token);

    void throwError(Token::TokenType expectedTokenType);
    void throwError(std::string msg);
    std::string getRuleTypeString(RuleType_t ruleType);
};


#endif //SYSPROGCOMPILER_PARSETREE_H
