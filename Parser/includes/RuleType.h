//
// Created by Henning on 01.04.2019.
//

#ifndef SYSPROGCOMPILER_RULETYPE_H
#define SYSPROGCOMPILER_RULETYPE_H

#include <string>

/* Strings to print out are available via ParseTree::getRuleTypeString(RuleType_t ruleType). Ugly, but works for now. */
enum RuleType_t {
    RULE_TYPE_PROG,         /* DECLS STATEMENTS */
    RULE_TYPE_DECLS,        /* DECL;DECLS | e */
    RULE_TYPE_DECL,         /* int ARRAY IDENTIFIER */
    RULE_TYPE_ARRAY,        /* [INTEGER] | e */

    RULE_TYPE_STATEMENTS,   /* STATEMENT;STATEMENTS | e */

    RULE_TYPE_STATEMENT_IDENTIFIER, /* IDENTIFIER INDEX := EXP */
    RULE_TYPE_STATEMENT_WRITE,      /* write(EXP) */
    RULE_TYPE_STATEMENT_READ,       /* read(IDENTIFIER INDEX) */
    RULE_TYPE_STATEMENT_BRACES,     /* {STATEMENTS} */
    RULE_TYPE_STATEMENT_IF,         /* if(EXP) STATEMENT else STATEMENT */
    RULE_TYPE_STATEMENT_WHILE,      /* while(EXP) STATEMENT */

    RULE_TYPE_EXP,          /* EXP2 OP_EXP */


    RULE_TYPE_EXP2_BRACES,      /* (EXP) */
    RULE_TYPE_EXP2_IDENTIFIER,  /* IDENTIFIER INDEX */
    RULE_TYPE_EXP2_INTEGER,     /* INTEGER */
    RULE_TYPE_EXP2_NEGATIVE,    /* -EXP2 */
    RULE_TYPE_EXP2_NEGATION,    /* !EXP2 */

    RULE_TYPE_INDEX,        /* [EXP] | e */
    RULE_TYPE_OP_EXP,       /* OP EXP | e */
    RULE_TYPE_OP,           /* + | - | * | : | < | > | = | =:= | && */

    RULE_TYPE_IDENTIFIER,   /* variable name */
    RULE_TYPE_INTEGER,      /* some number */

    RULE_TYPE_UNKNOWN,
    RULE_TYPE_ROOT
};

#endif //SYSPROGCOMPILER_RULETYPE_H
