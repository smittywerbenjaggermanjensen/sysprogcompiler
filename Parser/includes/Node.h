//
// Created by Henning on 01.04.2019.
//

#ifndef SYSPROGCOMPILER_NODE_H
#define SYSPROGCOMPILER_NODE_H

#include "../includes/RuleType.h"
#include "../../Automat/includes/Token.h"
#include "../includes/NodeType.h"

class Node {
public:
    Node(RuleType_t ruleType);

    void addChild(Node* child);
    Node* getChild(int index);

    void setParent(Node* parent);
    Node* getParent(void);

    bool isLeaf(void);

    RuleType_t getRuleType(void);

    void setToken(Token* token);
    Token* getToken(void);

    void setNodeType(NodeType nodeType);
    NodeType getNodeType(void);

protected:
    Node* parent;
    Node* children[1024];
    int childrenCount;

    RuleType_t ruleType;
    Token* token;
    NodeType nodeType;
};


#endif //SYSPROGCOMPILER_NODE_H
