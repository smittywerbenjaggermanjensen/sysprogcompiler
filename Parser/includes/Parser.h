#ifndef PARSER_H_
#define PARSER_H_

#include "../../Buffer/includes/Buffer.h"
#include "../../Scanner/includes/Scanner.h"
#include "../includes/ParseTree.h"
#include "../includes/TypeChecker.h"
#include "../includes/CodeGenerator.h"

class Parser {

public:
    Parser(Buffer *buffer);
    virtual ~Parser();
    void parse();

private:
    Scanner* scanner;
    Symboltable *symtab;
};

#endif /* PARSER_H_ */
