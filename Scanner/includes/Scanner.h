#ifndef SCANNER_H_
#define SCANNER_H_

#include "../../Automat/includes/Automat.h"
#include "../../Symboltable/includes/Symboltable.h"
#include "../../Buffer/includes/Buffer.h"

class Scanner {

public:
	Scanner(Buffer *buffer, Symboltable *symtab);
	virtual ~Scanner();
	Token *nextToken();

private:
	Buffer *buffer;
	Automat *automat;
	Symboltable *symboltable;
};

#endif /* SCANNER_H_ */
