#include "../includes/Scanner.h"

Scanner::Scanner(Buffer *buffer, Symboltable *symtab) {
    this->buffer = buffer;
    symboltable = symtab;
    automat = new Automat(buffer);

}

Scanner::~Scanner() {
    delete automat;
    delete symboltable;
    buffer = 0;
    automat = 0;
    symboltable = 0;
}

Token *Scanner::nextToken() {

    while (!automat->isFinished()) {
        CustomChar *custChar = buffer->getChar();

        if (custChar->getChar() == '\0' && automat->getState() == Automat::STATE_START) {
            Token *eofToken = new Token();
            eofToken->setTokenType(Token::END_OF_FILE);
            return eofToken;
        }

        automat->insertChar(custChar);
    }

    //getToken is always a new instance, so we do not need to copy it here
    Token *currentToken = automat->getToken();

    if (currentToken->getTokenType() == Token::IDENTIFIER) {
        SymbolTableKey *symbolTableKey = symboltable->insert(currentToken->getLexem(), currentToken->getLexemLength(),
                                                             currentToken->getTokenType());
        currentToken->setSymbolTableKey(symbolTableKey);
        currentToken->setTokenType(symboltable->lookup(symbolTableKey)->getTokenType());
    }
    return currentToken;
}
