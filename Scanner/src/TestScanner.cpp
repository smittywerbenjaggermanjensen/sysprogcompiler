#include "../includes/Scanner.h"
#include <fstream>
#include <iostream>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc < 3) {
        std::cout << "Usage: inputfile outputfile" << std::endl;
        return (EXIT_FAILURE);
    }

    //create output file stream
    std::ofstream outputFile(argv[2]);
    if (!outputFile.is_open()) {
        std::cerr << "Error opening '" << argv[2] << std::endl;
        return (EXIT_FAILURE);
    }

    Buffer *buffer = new Buffer(argv[1]);
    Symboltable *symboltable = new Symboltable();
    Scanner *scanner = new Scanner(buffer, symboltable);
    Token *token;

    while (true) {
        token = scanner->nextToken();
        if (token->getTokenType() == Token::END_OF_FILE) {
            outputFile.close();
            std::cout << "Scanner finished" << std::endl;
            return (EXIT_SUCCESS);
        }

        outputFile << "Row: " << token->getRow() << " Column: "
                   << token->getColumn();

        if (token->getTokenType() == Token::IDENTIFIER) {
            outputFile << " Type: Identifier Value: "
                       << token->getSymbolTableKey()->getLexem() << std::endl;

        } else if (token->getTokenType() == Token::NUMBER) {
            outputFile << " Type: Number Value: " << token->getNumber()
                       << std::endl;

        } else if (token->getTokenType() == Token::ERROR) {
            outputFile << " Type: Error Value: " << token->getSymbol()
                       << std::endl;

        } else {
            outputFile << " Type: " << token->getTokenTypeString() << std::endl;
        }
    }
}
